## Description

Prueba de Backend

## Instalación

### En la raíz del proyecto ejecutaremos el siguiente comando

```bash
$ npm install
```

## Formas de iniciar el server

```bash
# Desarrollador
$ npm run start

# Producción
$ npm run start:prod
```

## Postman
### En el proyecto hay una carpeta llamada postman que contiene la collection para las pruebas pertinentes

## Postman - IMPORTANTE
### Es importante recordar que toda la estructura del proyecto esta configurada con JWT, no olvidarse de poner el valor del token en las variables de Postman

## Base de datos

### Para el funcionamiento correcto de la prueba, se debe tener corriendo una instancia local de MongoDB con una base de datos llamada test, la URL está en el .env
