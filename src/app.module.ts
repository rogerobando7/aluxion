import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { ManagerModule } from './modules/manager/manager.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UnsplashModule } from './modules/unsplash/unsplash.module';
import { MailModule } from './modules/mail/mail.module';

@Module({
  imports: [
      ManagerModule,
      AuthModule,
      UnsplashModule,
      ConfigModule.forRoot(),
      MongooseModule.forRoot(process.env.URL_DATABASE),
      MailModule
  ],
  controllers: [],
  providers: [],
})

export class AppModule {}
