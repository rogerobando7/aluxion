import { BadRequestException, Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PhotosDto } from './dto/photos.dto';
import { UnsplashService } from './service/unsplash.service';

@Controller('unsplash')
export class UnsplashController {

    constructor(private unsplashService: UnsplashService){}

    @UseGuards(AuthGuard('jwt'))
    @Get('search/photos')
    async getPhotos(@Query() query : PhotosDto){
        try{
            return this.unsplashService.getPhotos(query)
        }catch(e){
            throw new BadRequestException(e)
        }
    }
}