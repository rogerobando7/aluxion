import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { map } from 'rxjs';
import { PhotosDto } from '../dto/photos.dto';

@Injectable()
export class UnsplashService {

    constructor(private readonly httpService : HttpService){}

    async getPhotos(photos : PhotosDto){
        try {
            let url = `${process.env.URL_UNSPLASH}/search/photos/?client_id=${process.env.ACCESS_KEY_UNPLASH}&query=${photos.name}`;
            return this.httpService.get(url).pipe(map(response => response.data));
        } catch (error) {
            throw new BadRequestException(error)
        }
    }
      
}
