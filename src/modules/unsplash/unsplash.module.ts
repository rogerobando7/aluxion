import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../auth/service/jwt.strategy';
import { UnsplashService } from './service/unsplash.service';
import {UnsplashController} from './unsplash.controller';

@Module({
  imports: [
    HttpModule,
    JwtModule.register({
      secret: '4luxion$',
      signOptions: { expiresIn: '3600s' }
    })],
    controllers: [UnsplashController],
    providers: [UnsplashService, JwtStrategy]
})
export class UnsplashModule {}
