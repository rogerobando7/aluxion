import { BadRequestException, Body, Controller, Get, Post, Query, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { DownloadDto } from './dto/download.dto';
import { FileDto } from './dto/file.dto';
import { ManagerService } from './service/manager.service';
import { Express } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { RenameDto } from './dto/rename.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('manager')
export class ManagerController {

    constructor(private managerService : ManagerService){}

    @UseGuards(AuthGuard('jwt'))
    @Post("upload/file")
    @UseInterceptors(FileInterceptor('file'))
    async uploadFile(@UploadedFile() file : Express.Multer.File, @Body() body : FileDto){
        try {
            body.buffer = file.buffer;
            return this.managerService.uploadFile(body)
        } catch (error) {
            throw new BadRequestException()
        }
    }

    @UseGuards(AuthGuard('jwt'))
    @Get("download/file")
    async downloadFile(@Query() downloadDto : DownloadDto){ 
        try {
            return this.managerService.downloadFile(downloadDto);
        } catch (error) {
            throw new BadRequestException()
        }
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("rename/file")
    async renameFile(@Body() renameDto : RenameDto){ 
        try {
            return this.managerService.renameFile(renameDto);
        } catch (error) {
            throw new BadRequestException()
        }
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("upload/url/file")
    async uploadUrlFile(@Body() str : String){
        try {
            return this.managerService.uploadUrlFile('')
        } catch (error) {
            throw new BadRequestException()
        }
    }
}
