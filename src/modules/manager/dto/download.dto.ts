import { IsNotEmpty } from "class-validator";

export class DownloadDto{

    @IsNotEmpty()
    name : string;

}