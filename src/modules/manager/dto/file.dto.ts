import { IsNotEmpty, IsString } from "class-validator";

export class FileDto{
    
    @IsNotEmpty()
    @IsString()
    originalname : string;
    
    @IsNotEmpty()
    @IsString()
    mimetype : string; 
    
    buffer : Buffer;

    url : string;

    constructor(){}
}