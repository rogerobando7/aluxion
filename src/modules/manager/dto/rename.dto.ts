import { IsNotEmpty } from "class-validator";

export class RenameDto{
    
    @IsNotEmpty()
    oldFileName : string;

    @IsNotEmpty()
    fileName : string;
}