import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../auth/service/jwt.strategy';
import { ManagerController } from './manager.controller';
import { ManagerService } from './service/manager.service';

@Module({
  imports: [
    JwtModule.register({
      secret: '4luxion$',
      signOptions: { expiresIn: '3600s' }
    })
  ],
  controllers: [ManagerController],
  providers: [ManagerService, JwtStrategy]
})
export class ManagerModule {}
