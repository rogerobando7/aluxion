import { BadRequestException, ConflictException, Injectable, Res } from '@nestjs/common';
import * as AWS from "aws-sdk";
import { DownloadDto } from '../dto/download.dto';
import * as fs from 'fs';
import { FileDto } from '../dto/file.dto';
import { RenameDto } from '../dto/rename.dto';

@Injectable()
export class ManagerService {

    private s3 = new AWS.S3({
        accessKeyId: process.env.AWS_S3_ACCESS_KEY,
        secretAccessKey: process.env.AWS_S3_KEY_SECRET,
    });

    async uploadFile(file : FileDto)
    {
        let params = {  Bucket: process.env.AWS_S3_BUCKET, 
                        Key: file.originalname,
                        Body: file.buffer };

        try
        {
            let s3Response = await this.s3.upload(params).promise();

            return {
                code : 200,
                data : s3Response,
                msg : "File upload to AWS successfully"
            }
        }
        catch (e)
        {
            throw new BadRequestException()
        }
    }

    async renameFile(renameDto : RenameDto){
        try{
            var copyParams = {
                Bucket: process.env.AWS_S3_BUCKET, 
                CopySource: `/${process.env.AWS_S3_BUCKET}/${renameDto.oldFileName}`, 
                Key: `${renameDto.fileName}`
            };    

            await this.s3.copyObject(copyParams).promise();

            const deleteParams = {
                Bucket : process.env.AWS_S3_BUCKET,
                Key : `/${process.env.AWS_S3_BUCKET}/${renameDto.oldFileName}`
            };

            await this.s3.deleteObject(deleteParams).promise();

            let locationObject = (await this.s3.getObject({ Bucket : process.env.AWS_S3_BUCKET,
                                                            Key : renameDto.fileName}).promise()).Body

            return { code : 200, msg : "File rename successfully", file : locationObject };
            
        }catch(error){
            throw new BadRequestException(error)
        } 
    }

    async downloadFile(downloadDto : DownloadDto){
        try {
            let params = {
                Bucket : process.env.AWS_S3_BUCKET,
                Key : downloadDto.name,
            }
            let s3Object = (await this.s3.getObject(params).promise()).Body;
            if(s3Object){
                
                let fileStream = this.s3.getObject(params).createReadStream();
                let location = `${process.cwd()}/public/${downloadDto.name}`;
                const file = fs.createWriteStream(location);
                fileStream.pipe(file);
                
                return { code : 200, data: { location , name : downloadDto.name } }    
            }
        } catch (error) {
            throw new ConflictException(error.message)
        }
    }


    async uploadUrlFile(urlFile : string){
        // https://docs.nestjs.com/assets/logo-small.svg
    }
}
