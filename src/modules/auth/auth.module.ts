import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthController } from './auth.controller';
import { AuthService } from './service/auth.service';
import { User, UserSchema } from '../../schemas/user';
import { EncryptionService } from 'src/utils/encryption.service';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './service/jwt.strategy';


@Module({
  imports: [
    MongooseModule.forFeatureAsync([{ 
    name : User.name, 
    useFactory: function(){
      const schema = UserSchema 
      let service = new EncryptionService();
      schema.pre('save', function(){
          var user = this;
          user.password = service.encodedPassword(user.password);
      })
      return schema;
    },
    imports: [EncryptionService]
    }]),
    JwtModule.register({
      secret: '4luxion$',
      signOptions: { expiresIn: '3600s' }
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, EncryptionService, JwtStrategy]
})
export class AuthModule {}
