import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { SignupDto } from './dto/signup.dto';
import { SigninDto } from './dto/signin.dto';
import { AuthService } from './service/auth.service';
import { ProfileDto } from './dto/profile.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {

    constructor(private authService : AuthService){}

    @Post("signin")
    async signin(@Body() signinDto : SigninDto){
        return this.authService.signin(signinDto)
    }

    @Post("signup")
    async register(@Body() signupDto : SignupDto){
        return this.authService.register(signupDto)
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("profile")
    async profile(@Body() profileDto : ProfileDto){
        return this.authService.profile(profileDto)
    }
}
