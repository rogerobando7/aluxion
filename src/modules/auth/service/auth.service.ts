import { BadRequestException, ConflictException, Injectable, UnauthorizedException } from '@nestjs/common';
import { SignupDto } from '../dto/signup.dto';
import { SigninDto } from '../dto/signin.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../../../schemas/user';
import { Model } from 'mongoose';
import { EncryptionService } from 'src/utils/encryption.service';
import { JwtService } from '@nestjs/jwt';
import { ProfileDto } from '../dto/profile.dto';
import { MailService } from 'src/modules/mail/service/mail.service';

@Injectable()
export class AuthService {

    constructor(@InjectModel(User.name) private userModel : Model<UserDocument>,
                private encryptionService : EncryptionService,
                private mailService : MailService,
                private jwtTokenService: JwtService){}

    async signin(signinDto : SigninDto){
        try{
            const user = await this.userModel.find({email : signinDto.email});
            if(user.length == 0)
                throw new Error("User not found")
            
            let userFind : User = user[0];
            const jwtPayload = { email : userFind.email, name : userFind.name }
            let matchPassword = this.encryptionService.compareHash(signinDto.password, userFind.password)
            if(!matchPassword)
                throw new Error("Credentials incorrect")
            this.mailService.sendUserConfirmation({email:userFind.email, name:userFind.name})
            return { code : 200 , msg : "User signed succesfully", data : { accessToken : this.jwtTokenService.sign(jwtPayload) } }
            
        }catch(error){
            throw new BadRequestException(error.message)
        }
    }

    async register(signupDto : SignupDto){
        try {
            let userModel = new this.userModel(signupDto)
            await userModel.save()
            return {code : 200, msg : "User created succesfully"}
        } catch (error) {
            throw new BadRequestException(error)
        }       
    }


    async profile(profileDto : ProfileDto){
        try {
            const user = await this.userModel.find({email : profileDto.email});
            if(user.length == 0)
                throw new Error("User not found")

            let { email, lastName , name} = user[0]

            return { code : 200 , data : { user: { email, lastName, name} } }

        } catch (error){
            throw new BadRequestException(error.message)            
        }
    }
}
