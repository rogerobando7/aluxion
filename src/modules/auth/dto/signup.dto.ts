import { IsNotEmpty, IsString, Length } from "class-validator";

export class SignupDto{
    
    @IsNotEmpty()
    @IsString()
    name : string;
    
    @IsNotEmpty()
    @IsString()
    lastName : string;
    
    @IsNotEmpty()
    @IsString()
    email : string;

    @IsNotEmpty()
    @IsString()
    @Length(5, 12)
    password : string;


}