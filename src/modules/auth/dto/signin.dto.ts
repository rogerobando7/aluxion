import { IsEmail, IsNotEmpty, IsString, Length } from "class-validator";

export class SigninDto{
    
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    email : string;

    @IsNotEmpty()
    @IsString()
    @Length(5, 12)
    password : string;
}