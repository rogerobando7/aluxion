import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { MailDto } from '../dto/mail.dto';  

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(mailDto: MailDto) {
    this.mailerService
      .sendMail({
        to: mailDto.email, // list of receivers
        from: 'messegeralux@gmail.com', // sender address
        subject: 'Messenger Alux', // Subject line
        html: '<b>Bienvenido a la prueba</b>', // HTML body content
      })
      .then((data) => {})
      .catch((error) => {});
  }
}
