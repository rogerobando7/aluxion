import * as bcrypt from 'bcrypt';

export class EncryptionService{
    
    constructor(){}

    encodedPassword(password : string){
        const SALT = bcrypt.genSaltSync();
        let hash = bcrypt.hashSync(password, SALT);
        return hash;
    }

    compareHash(password : string, hash : string){
        return bcrypt.compareSync(password, hash)
    }
}